package ee.lessons.privatelessons.resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;



import ee.lessons.privatelessons.resource.DatabaseConnection;
import ee.lessons.privatelessons.dao.Parish;

public abstract class ParishResource {
	 
	public static LinkedHashSet<Parish> getAllParishes() {
		LinkedHashSet<Parish> parishes = new LinkedHashSet<>();
		String sqlQuery = "SELECT * FROM parish ORDER BY name";
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				Parish parish = new Parish().setId(results.getInt("id")).setCounty_id(results.getInt("county_id")).setName(results.getString("name"));

				parishes.add(parish);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting parish set " + e.getMessage());
		}
		return parishes;
	}

	public static Parish getParishById(int parishID) {
		Parish parish = null;
		String sqlQuery = "SELECT * FROM parish WHERE id = " + parishID;
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				parish = new Parish().setId(parishID).setCounty_id(results.getInt("county_id"))
						.setName(results.getString("name"));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting parish set: " + e.getMessage());
		}
		return parish;
	}

	public static LinkedHashSet<Parish> getParishByCounty(int countyID) {
		LinkedHashSet<Parish> parishes = new LinkedHashSet<>();
		Parish parish = null;
		String sqlQuery = "SELECT * FROM parish WHERE  county_id = " + countyID + " ORDER BY name ";
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				parish = new Parish().setId(results.getInt("id")).setCounty_id(results.getInt("county_id"))
						.setName(results.getString("name"));
				parishes.add(parish);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting parish set: " + e.getMessage());
		}
		return parishes;
	}	
	
}


