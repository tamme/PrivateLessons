package ee.lessons.privatelessons.resource;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnection {
	private static Connection connection = null; // connection on db handler

	public static Connection getConnection() {
		String dbUrl = "jdbc:mysql://localhost:3306/private_lessons";
		Properties connectionProperties = new Properties(); // laiendab hashtablet, eritüüpi hashtable minu jaoks
		connectionProperties.put("user", "root");
		connectionProperties.put("password", "parool");
		loadDriver(); // laeb draiveri (tomcat serveri jaoks)
		try {
			connection = DriverManager.getConnection(dbUrl, connectionProperties);
		} catch (SQLException e) {
			System.out.println("Error on createing databae connection: " + e.getMessage());
		}
		return connection;
	}
	private static void loadDriver() {   // laeb draiveri (tomcat serveri jaoks)
	try {
		Class.forName("com.mysql.cj.jdbc.Driver").getConstructor().newInstance(); // multi catch
	} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
			| NoSuchMethodException | SecurityException | ClassNotFoundException e) {
		System.out.println("Error on loading driver: " + e.getMessage());
	}
}
	public static void closeConnection(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			System.out.println("Error on close databae connection: " + e.getMessage());
		}
	}
}

