package ee.lessons.privatelessons.resource;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;


import ee.lessons.privatelessons.resource.DatabaseConnection;
import ee.lessons.privatelessons.dao.User;

public abstract class UserResource {

	public static Set<User> getAllUsers() {
		Set<User> users = new HashSet<>();
		String sqlQuery = "SELECT * FROM user";
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				User user = new User().setId(results.getInt("id")).setFirstname(results.getString("firstname"))
						.setLastname(results.getString("lastname")).setBirthday(results.getDate("birthday"))
						.setAddressId(results.getInt("address_id")).setPhone(results.getInt("phone"))
						.setEmail(results.getString("email")).setUsername(results.getString("username"))
						.setPassword(results.getString("password"));

				users.add(user);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting user set " + e.getMessage());
		}
		return users;
	}

	public static User getUserById(int userID) {
		User user = null;
		String sqlQuery = "SELECT * FROM user WHERE id = " + userID;
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				user = new User().setId(results.getInt("id"))
						.setFirstname(results.getString("firstname"))
						.setLastname(results.getString("lastname"))
						.setBirthday(results.getDate("birthday"))
						.setAddressId(results.getInt("address_id"))
						.setPhone(results.getInt("phone"))
						.setEmail(results.getString("email"))
						.setUsername(results.getString("username"))
						.setPassword(results.getString("password"));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting user set: " + e.getMessage());
		}

		return user;
	}

	public static User getUserByUsername(String username) {
		User user = null;
		String sqlQuery = "SELECT * FROM user WHERE username=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setString(1, username);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				 user = new User().setId(results.getInt("id")).setFirstname(results.getString("firstname"))
							.setLastname(results.getString("lastname")).setBirthday(results.getDate("birthday"))
							.setAddressId(results.getInt("address_id")).setPhone(results.getInt("phone"))
							.setEmail(results.getString("email")).setUsername(results.getString("username"))
							.setPassword(results.getString("password"));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting address set: " + e.getMessage());
		}
		return user;
	}
	
	
	
	private static Date javaDateToSqlDate(java.util.Date date) {
		if (date == null) {
			return null;
		} else {
			return new java.sql.Date(date.getTime());
		}
	}

	public static void updateUserBy(User user) {
		String sqlQuery = "UPDATE user SET firstname = ?, lastname = ?, birthday = ?, address_id = ?, phone = ?, email = ?, username = ?, password = ? WHERE id = ?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
			statement.setString(1, user.getFirstname());
			statement.setString(2, user.getLastname());
			statement.setDate(3, javaDateToSqlDate(user.getBirthday()));
			statement.setInt(4, user.getAddressId());
			statement.setInt(5, user.getPhone());
			statement.setString(6, user.getEmail());
			statement.setString(7, user.getUsername());
			statement.setString(8, user.getPassword());
			statement.setInt(9, user.getId());
			statement.executeUpdate();
		
		} catch (SQLException e) {
			System.out.println("Error on updating user: " + e.getMessage());
		}

	}

	public static User addUser(User user) {
		String sqlQuery = "INSERT INTO user (firstname, lastname, birthday, address_id, phone, email, username, password) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery,
				Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, user.getFirstname());
			statement.setString(2, user.getLastname());
			statement.setDate(3, javaDateToSqlDate(user.getBirthday()));
			statement.setInt(4, user.getAddressId());
			statement.setInt(5, user.getPhone());
			statement.setString(6, user.getEmail());
			statement.setString(7, user.getUsername());
			statement.setString(8, user.getPassword());
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				user.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println("Error on adding user: " + e.getMessage());
		}
		return user;
	}

}
