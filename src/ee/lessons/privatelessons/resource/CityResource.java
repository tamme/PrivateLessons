package ee.lessons.privatelessons.resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import ee.lessons.privatelessons.resource.DatabaseConnection;
import ee.lessons.privatelessons.dao.City;

public abstract class CityResource {

	public static LinkedHashSet<City> getAllCities() {
		LinkedHashSet<City> cities = new LinkedHashSet<>();
		String sqlQuery = "SELECT * FROM city ORDER BY name";
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				City city = new City().setId(results.getInt("id")).setCounty_id(results.getInt("county_id")).setName(results.getString("name"))
						.setParish_id(results.getInt("parish_id"));

				cities.add(city);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting city set " + e.getMessage());
		}
		return cities;
	}

	public static City getCityById(int cityID) {
		City city = null;
		String sqlQuery = "SELECT * FROM city WHERE id = " + cityID;
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				city = new City().setId(cityID).setCounty_id(results.getInt("county_id"))
						.setName(results.getString("name")).setParish_id(results.getInt("parish_id"));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting city set: " + e.getMessage());
		}
		return city;
	}

	public static LinkedHashSet<City> getCityByCounty(int countyID) {
		LinkedHashSet<City> cities = new LinkedHashSet<>();
		City city = null;
		String sqlQuery = "SELECT * FROM city WHERE county_id = " + countyID + " ORDER BY name "; 
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				city = new City().setId(results.getInt("id")).setCounty_id(results.getInt("county_id"))
						.setName(results.getString("name")).setParish_id(results.getInt("parish_id"));
				cities.add(city);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting city set: " + e.getMessage());
		}
		return cities;
	}	
	
	// get city by parish_id
	public static LinkedHashSet<City> getCityByParish(int parishID) {
		LinkedHashSet<City> cities = new LinkedHashSet<>();

		String sqlQuery = "SELECT * FROM city ORDER BY name WHERE parish_id = " + parishID;
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				City city = new City().setId(results.getInt("id")).setCounty_id(results.getInt("county_id"))
						.setName(results.getString("name")).setParish_id(results.getInt("parish_id"));
				cities.add(city);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting city set: " + e.getMessage());
		}
		return cities;
	}	
	
	
}
