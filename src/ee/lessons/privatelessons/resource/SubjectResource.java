package ee.lessons.privatelessons.resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import ee.lessons.privatelessons.dao.Subject;

public abstract class SubjectResource {
	public static LinkedHashSet<Subject> getAllSubjects() {
		LinkedHashSet<Subject> subjects = new LinkedHashSet<>();
		String sqlQuery = "SELECT * FROM Subject ORDER BY name";
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				Subject Subject = new Subject().setId(results.getInt("id")).setName(results.getString("name"));

				subjects.add(Subject);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting Subject set " + e.getMessage());
		}
		return subjects;
	}

	public static Subject getSubjectById(int SubjectID) {
		Subject subject = null;
		String sqlQuery = "SELECT * FROM Subject WHERE id = " + SubjectID;
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				subject = new Subject().setId(SubjectID)
						.setName(results.getString("name"));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting Subject set: " + e.getMessage());
		}
		return subject;
	}
}
