package ee.lessons.privatelessons.resource;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.LinkedHashSet;

import ee.lessons.privatelessons.resource.DatabaseConnection;
import ee.lessons.privatelessons.dao.County;

public abstract class CountyResource {

	public static LinkedHashSet<County> getAllCounties() {
		LinkedHashSet<County> counties = new LinkedHashSet<>();
		String sqlQuery = "SELECT * FROM county ORDER BY name";
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				County county = new County().setId(results.getInt("id")).setName(results.getString("name"));

				counties.add(county);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting county set " + e.getMessage());
		}
		return counties;
	}

	public static County getcountyById(int countyID) {
		County county = null;
		String sqlQuery = "SELECT name FROM county WHERE id = " + countyID;
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				county = new County().setId(countyID)
						.setName(results.getString("name"));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting county set: " + e.getMessage());
		}
		return county;
	}
	
}