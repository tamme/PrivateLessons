package ee.lessons.privatelessons.resource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashSet;
import ee.lessons.privatelessons.dao.LessonOffer;

public abstract class LessonOfferResource {
	static String filterSubject;
	static String filterCounty;
	static String filterWish;
	static String filterUserId;

	public static LinkedHashSet<LessonOffer> getLessonOffersWithFilters(LessonOffer lessonOffer) {
		LinkedHashSet<LessonOffer> lessonoffers = new LinkedHashSet<>();
		System.out.println("subject id on " + lessonOffer.getSubjectId());
		System.out.println(lessonOffer);

		if (lessonOffer.getSubjectId() > 0) {
			filterSubject = " AND subject_id = " + lessonOffer.getSubjectId() + " ";
		} else {
			filterSubject = "";
		}

		if (lessonOffer.getCountyId() > 0) {
			filterCounty = " AND county_id = " + lessonOffer.getCountyId() + " ";
		} else {
			filterCounty = "";
		}
		
		if (lessonOffer.getUserId() > 0) {
			filterUserId = " AND user_id = " + lessonOffer.getUserId() + " ";
		}
		else {
			filterUserId = "";
		}
		
		System.out.println(lessonOffer.getWish());
		
		if (lessonOffer.getWish() == 1) {
			filterWish = " AND wish = true ";
			}
		else if (lessonOffer.getWish() == 0) { 
			filterWish = " AND wish = false "; 
			}
		else { 
			filterWish = ""; 
			}
		
		String sqlQuery = "SELECT *, " + "(((acos(sin((?*pi()/180)) * sin((`lat`*pi()/180))+cos((?*pi()/180)) * "
				+ " cos((`lat`*pi()/180)) * cos(((?- `lng`) * pi()/180))))*180/pi())*60*1.1515 )*1.609344 as km "
				+ "FROM offers WHERE 1 = 1 " + filterSubject + filterCounty + filterWish + filterUserId +";";

		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setBigDecimal(1, lessonOffer.getLat());
			statement.setBigDecimal(2, lessonOffer.getLat());
			statement.setBigDecimal(3, lessonOffer.getLng());
			ResultSet results = statement.executeQuery();
			System.out.println(statement);
			while (results.next()) {
				lessonoffers.add(new LessonOffer().setId(results.getInt("id"))
						.setSubjectId(results.getInt("subject_id")).setDescription(results.getString("description"))
						.setAddressId(results.getInt("address_id")).setDistance(results.getInt("distance"))
						.setGroupsize(results.getInt("groupsize")).setPrice(results.getBigDecimal("price"))
						.setUserId(results.getInt("user_id")).setFirstName(results.getString("firstName"))
						.setLastName(results.getString("lastName")).setSubjectName(results.getString("subjectName"))
						.setCountyName(results.getString("countyName")).setParishName(results.getString("parishName"))
						.setCityName(results.getString("cityName")).setStreet(results.getString("street"))
						.setUserName(results.getString("username")).setEmail(results.getString("email"))
						.setPhone(results.getInt("phone")).setBirthDay(results.getDate("birthDay"))
						.setWish(results.getInt("wish")).setAge(results.getInt("age"))
						.setLat(results.getBigDecimal("lat")).setLng(results.getBigDecimal("lng"))
						.setKm(results.getInt("km")).setMinutes(results.getInt("minutes")));

			}

		} catch (SQLException e) {
			System.out.println("Error on Lesson offers set " + e.getMessage());
		}
		return lessonoffers;
	}

	public static LinkedHashSet<LessonOffer> getJoinedAllLessonOffers() {
		LinkedHashSet<LessonOffer> lessonoffers = new LinkedHashSet<>();
		String sqlQuery = "SELECT" + "  subject.name AS subjectName," + "  county.name AS countyName,"
				+ "  parish.name AS parishName," + "  city.name AS cityName," + "  user.firstname,"
				+ "  DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birthday)), \"%Y\")+0 AS age," + "  user.lastname,"
				+ "  user.birthDay," + "  user.phone," + "  user.email," + "  user.username, "
				+ "  lesson_offer.subject_id," + "  lesson_offer.id," + "  lesson_offer.description,"
				+ "  lesson_offer.address_id," + "  lesson_offer.distance," + "  lesson_offer.price, "
				+ "  lesson_offer.groupsize, " + "  lesson_offer.minutes, " + "  lesson_offer.user_id, "
				+ "  address.lat, " + "  address.lng, "
				+ "  (((acos(sin((58.3810843*pi()/180)) * sin((`lat`*pi()/180))+cos((58.3810843*pi()/180)) * "
				+ "  cos((`lat`*pi()/180)) * cos(((26.7198659- `lng`) * pi()/180))))*180/pi())*60*1.1515 )*1.609344 as km, "
				+ "  lesson_offer.wish " + "FROM lesson_offer "
				+ "LEFT JOIN subject ON lesson_offer.subject_id = subject.id "
				+ "LEFT JOIN address ON lesson_offer.address_id = address.id "
				+ "LEFT JOIN county ON address.county_id = county.id "
				+ "LEFT JOIN parish ON address.parish_id = parish.id " + "LEFT JOIN city ON address.city_id = city.id "
				+ "LEFT JOIN user ON lesson_offer.user_id = user.id WHERE lesson_offer.wish = 0";

		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				lessonoffers.add(new LessonOffer().setId(results.getInt("id"))
						.setSubjectId(results.getInt("subject_id")).setDescription(results.getString("description"))
						.setAddressId(results.getInt("address_id")).setDistance(results.getInt("distance"))
						.setGroupsize(results.getInt("groupsize")).setPrice(results.getBigDecimal("price"))
						.setUserId(results.getInt("user_id")).setFirstName(results.getString("firstName"))
						.setLastName(results.getString("lastName")).setSubjectName(results.getString("subjectName"))
						.setCountyName(results.getString("countyName")).setParishName(results.getString("parishName"))
						.setUserName(results.getString("username")).setEmail(results.getString("email"))
						.setPhone(results.getInt("phone")).setBirthDay(results.getDate("birthDay"))
						.setWish(results.getInt("wish")).setAge(results.getInt("age"))
						.setLat(results.getBigDecimal("lat")).setLng(results.getBigDecimal("lng"))
						.setKm(results.getInt("km")).setMinutes(results.getInt("minutes")));

			}

		} catch (SQLException e) {
			System.out.println("Error on Lesson offers set " + e.getMessage());
		}

		return lessonoffers;
	}

	public static LinkedHashSet<LessonOffer> getJoinedAllLessonWishes() {
		LinkedHashSet<LessonOffer> lessonoffers = new LinkedHashSet<>();
		String sqlQuery = "SELECT" + "  subject.name AS subjectName," + "  county.name AS countyName,"
				+ "  parish.name AS parishName," + "  city.name AS cityName," + "  user.firstname," + "  user.lastname,"
				+ "  user.birthday," + "  DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), user.birthday)), \"%Y\")+0 AS age,"
				+ "  user.phone," + "  user.email," + "  user.username, " + "  lesson_offer.subject_id,"
				+ "  lesson_offer.id," + "  lesson_offer.description," + "  lesson_offer.address_id,"
				+ "  lesson_offer.distance," + "  lesson_offer.price, " + "  lesson_offer.groupsize, "
				+ "  lesson_offer.minutes, " + "  lesson_offer.user_id, " + "  address.lat, " + "  address.lng, "
				+ "  (((acos(sin((58.3810843*pi()/180)) * sin((`lat`*pi()/180))+cos((58.3810843*pi()/180)) * "
				+ "  cos((`lat`*pi()/180)) * cos(((26.7198659- `lng`) * pi()/180))))*180/pi())*60*1.1515 )*1.609344 as km, "
				+ "  lesson_offer.wish " + "FROM lesson_offer "
				+ "LEFT JOIN subject ON lesson_offer.subject_id = subject.id "
				+ "LEFT JOIN address ON lesson_offer.address_id = address.id "
				+ "LEFT JOIN county ON address.county_id = county.id "
				+ "LEFT JOIN parish ON address.parish_id = parish.id " + "LEFT JOIN city ON address.city_id = city.id "
				+ "LEFT JOIN user ON lesson_offer.user_id = user.id WHERE lesson_offer.wish = 1";
		System.out.println(sqlQuery);
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				lessonoffers.add(new LessonOffer().setId(results.getInt("id"))
						.setSubjectId(results.getInt("subject_id")).setDescription(results.getString("description"))
						.setAddressId(results.getInt("address_id")).setDistance(results.getInt("distance"))
						.setGroupsize(results.getInt("groupsize")).setPrice(results.getBigDecimal("price"))
						.setUserId(results.getInt("user_id")).setFirstName(results.getString("firstName"))
						.setLastName(results.getString("lastName")).setSubjectName(results.getString("subjectName"))
						.setCountyName(results.getString("countyName")).setParishName(results.getString("parishName"))
						.setUserName(results.getString("username")).setEmail(results.getString("email"))
						.setPhone(results.getInt("phone")).setBirthDay(results.getDate("birthDay"))
						.setWish(results.getInt("wish")).setLat(results.getBigDecimal("lat"))
						.setAge(results.getInt("age")).setLng(results.getBigDecimal("lng")).setKm(results.getInt("km"))
						.setMinutes(results.getInt("minutes")));

			}

		} catch (SQLException e) {
			System.out.println("Error on Lesson offers set " + e.getMessage());
		}

		return lessonoffers;
	}

	public static LinkedHashSet<LessonOffer> getAllLessonOffers() {
		LinkedHashSet<LessonOffer> lessonoffers = new LinkedHashSet<>();
		String sqlQuery = "SELECT * FROM lesson_offer";

		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				lessonoffers.add(new LessonOffer().setId(results.getInt("id"))
						.setSubjectId(results.getInt("subject_id")).setDescription(results.getString("description"))
						.setAddressId(results.getInt("address_id")).setDistance(results.getInt("distance"))
						.setGroupsize(results.getInt("groupsize")).setPrice(results.getBigDecimal("price"))
						.setUserId(results.getInt("user_id")).setMinutes(results.getInt("minutes")));
			}

		} catch (SQLException e) {
			System.out.println("Error on Lesson offers set " + e.getMessage());
		}

		return lessonoffers;
	}

	public static LessonOffer addLessonOffer(LessonOffer lessonOffer) {
		String sqlQuery = "INSERT INTO lesson_offer (subject_id, description, address_id, distance, groupsize, price, minutes, user_id, wish) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery,
				Statement.RETURN_GENERATED_KEYS)) {
			statement.setInt(1, lessonOffer.getSubjectId());
			statement.setString(2, lessonOffer.getDescription());
			statement.setInt(3, lessonOffer.getAddressId());
			statement.setInt(4, lessonOffer.getDistance());
			statement.setInt(5, lessonOffer.getGroupsize());
			statement.setBigDecimal(6, lessonOffer.getPrice());
			statement.setInt(7, lessonOffer.getMinutes());
			statement.setInt(8, lessonOffer.getUserId());
			statement.setInt(9, lessonOffer.getWish());

			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				lessonOffer.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println("Error on adding lesson offer: " + e.getMessage());
		}
		return lessonOffer;
	}

	public static void updateLessonOfferBy(LessonOffer lessonOffer) {
		String sqlQuery = "UPDATE lessonOffer SET subject_id = ?, description = ?, address_id = ?, distance = ?, groupsize = ?, price = ?, minutes = ?, user_id = ?, wish = ? WHERE id = ?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
			statement.setInt(1, lessonOffer.getSubjectId());
			statement.setString(2, lessonOffer.getDescription());
			statement.setInt(3, lessonOffer.getAddressId());
			statement.setInt(4, lessonOffer.getDistance());
			statement.setInt(5, lessonOffer.getGroupsize());
			statement.setBigDecimal(6, lessonOffer.getPrice());
			statement.setInt(7, lessonOffer.getMinutes());
			statement.setInt(8, lessonOffer.getUserId());
			statement.setInt(9, lessonOffer.getWish());

			statement.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Error on updating lessonOffer: " + e.getMessage());
		}

	}

	public static void deleteLessonOffer(LessonOffer lessonOffer) {
		deleteLessonOfferById(lessonOffer.getId());
	}

	public static void deleteLessonOfferById(int lessonOfferId) {
		String sqlQuery = "DELETE from lesson_offer WHERE id =?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, lessonOfferId);
			Integer code = statement.executeUpdate();
			if (code != 1) {
				throw new SQLException("Something went wrong on deleting lesson offer");
			}
		} catch (SQLException e) {
			System.out.println("Error on deleting lesson offer: " + e.getMessage());
		}
	}
}
