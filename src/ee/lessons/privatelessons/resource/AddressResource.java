package ee.lessons.privatelessons.resource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;
import ee.lessons.privatelessons.resource.DatabaseConnection;
import ee.lessons.privatelessons.dao.Address;


public abstract class AddressResource { //

	public static Set<Address> getAllAddresses() {
		Set<Address> addresses = new HashSet<>();
		String sqlQuery = "SELECT address.id, address.city_id, address.county_id, address.parish_id,"
				+ "  address.street, address.lat, address.lng, parish.name AS parishName, county.name AS countyName, city.name AS cityName"
				+ " FROM address" + " LEFT JOIN city ON city.id = address.city_id"
				+ "  LEFT JOIN parish ON parish.id = address.parish_id"
				+ "  LEFT JOIN county ON county.id = address.county_id";
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				Address address = new Address().setId(results.getInt("address.id"))
						.setCityId(results.getInt("address.city_id")).setCountyId(results.getInt("address.county_id"))
						.setParishId(results.getInt("address.parish_id")).setStreet(results.getString("address.street"))
						.setCityName(results.getString("cityName")).setCountyName(results.getString("countyName"))
						.setParishName(results.getString("parishName")).setLat(results.getBigDecimal("lat"))
						.setLng(results.getBigDecimal("lng"));

				addresses.add(address);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting address set " + e.getMessage());
		}
		return addresses;
	}

	public static Address getAddressById(int addressID) {
		Address address = null;
		String sqlQuery = "SELECT address.id, address.city_id, address.county_id, address.parish_id,"
				+ " address.street, address.lng, address.lat, parish.name AS parishName, county.name AS countyName, city.name AS cityName"
				+ " FROM address" + " LEFT JOIN city ON city.id = address.city_id"
				+ " LEFT JOIN parish ON parish.id = address.parish_id"
				+ " LEFT JOIN county ON county.id = address.county_id WHERE address.id = " + addressID;
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				address = new Address().setId(results.getInt("address.id"))
						.setCityId(results.getInt("address.city_id"))
						.setCountyId(results.getInt("address.county_id"))
						.setParishId(results.getInt("address.parish_id"))
						.setStreet(results.getString("address.street"))
						.setCityName(results.getString("cityName"))
						.setCountyName(results.getString("countyName"))
						.setParishName(results.getString("parishName"))
						.setLat(results.getBigDecimal("lat"))
						.setLng(results.getBigDecimal("lng"));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting address set: " + e.getMessage());
		}

		return address;
	}


	public static Set<Address> getAddressBySubjectId(int subjectID) {
		Set<Address> addresses = new HashSet<>();
		String sqlQuery = "SELECT * FROM address WHERE subject_id = " + subjectID;
		ResultSet results;
		try {
			results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);
			while (results.next()) {
				Address address = new Address().setId(results.getInt("id")).setCityId(results.getInt("city_id"))
						.setCountyId(results.getInt("county_id")).setParishId(results.getInt("parish_id"))
						.setStreet(results.getString("street"));
				addresses.add(address);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting address: " + e.getMessage());
		}

		return addresses;
	}

	public static Address addAddress(Address address) {
		String sqlQuery = "INSERT INTO address (city_id, county_id, parish_id, street, lat, lng) VALUES (?, ?, ?, ?, ?, ?)";

		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery,
				Statement.RETURN_GENERATED_KEYS)) {
			statement.setInt(1, address.getCityId());
			statement.setInt(2, address.getCountyId());
			statement.setInt(3, address.getParishId());
			statement.setString(4, address.getStreet());
			statement.setBigDecimal(5, address.getLat());
			statement.setBigDecimal(6, address.getLng());
			
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			
			while (resultSet.next()) {
				address.setId(resultSet.getInt(1));

			}
		} catch (SQLException e) {
			System.out.println("Error on adding new address row: " + e.getMessage());
		}
		return address;
	}

	
	public static void deleteAddressByID(Address address) {
		String sqlQuery = "DELETE FROM address WHERE id = " + address.getId();
		try {
			DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
		} catch (SQLException e) {
			System.out.println("Error on delete");
		}
	}


	public static void updateAddressBy(Address address) {
		String sqlQuery = "UPDATE address SET city_id = ?, county_id = ?, parish_id = ?, street = ?, lat = ?, lng = ? WHERE id = ?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery);) {
			statement.setInt(1, address.getCityId());
			statement.setInt(2, address.getCountyId());
			statement.setInt(3, address.getParishId());
			statement.setString(4, address.getStreet());
			statement.setBigDecimal(5, address.getLat());
			statement.setBigDecimal(6, address.getLng());
			statement.setInt(7, address.getId());
			
			statement.executeUpdate();
			
			
		} catch (SQLException e) {
			System.out.println("Error on update " + e.getMessage() + sqlQuery);
		}
	}
}
