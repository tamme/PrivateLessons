package ee.lessons.privatelessons.controller;

import java.util.Set;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ee.lessons.privatelessons.dao.Parish;
import ee.lessons.privatelessons.resource.ParishResource;

@Path("/parishes")
public class ParishController {
	@GET
	@Produces (MediaType.APPLICATION_JSON)
	public Set<Parish> getAllParishes() {
		return ParishResource.getAllParishes();
	}

	@GET
	@Path("/{parishId}")
	@Produces (MediaType.APPLICATION_JSON)
	public Set<Parish>  getParishByCounty(@PathParam("parishId") int countyId) {
		return ParishResource.getParishByCounty(countyId);

	}
	
	@GET
	@Path("/byid/{parishID}")
	@Produces (MediaType.APPLICATION_JSON)
	public Parish getParishById(@PathParam("parishID") int parishID) {
		return ParishResource.getParishById(parishID);
	}	
	

}

