package ee.lessons.privatelessons.controller;

import java.util.LinkedHashSet;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ee.lessons.privatelessons.dao.LessonOffer;
import ee.lessons.privatelessons.resource.LessonOfferResource;

@Path("/lessons")
public class LessonOfferController {

	
	
	@GET
	@Path("/offers")
	@Produces (MediaType.APPLICATION_JSON) 
	public LinkedHashSet<LessonOffer> getJoinedAllLessonOffers() {
		return LessonOfferResource.getJoinedAllLessonOffers();
	}	
	@GET
	@Path("/wishes")
	@Produces (MediaType.APPLICATION_JSON)
	public LinkedHashSet<LessonOffer> getJoinedAllLessonWishes() {
		return LessonOfferResource.getJoinedAllLessonWishes();
	}	
	
	@GET
	@Produces (MediaType.APPLICATION_JSON) 
	public LinkedHashSet<LessonOffer> getAllLessonOffers() {
		return LessonOfferResource.getAllLessonOffers();
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void putLessonOffer(LessonOffer lessonOffer) {
		LessonOfferResource.updateLessonOfferBy(lessonOffer);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)		
	@Consumes(MediaType.APPLICATION_JSON)		
	public LessonOffer addLessonOffer(LessonOffer lessonOffer) {
		return LessonOfferResource.addLessonOffer(lessonOffer);
	}	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)		
	@Consumes(MediaType.APPLICATION_JSON)	
	@Path("/filters")
	public LinkedHashSet<LessonOffer> getLessonWithFilters(LessonOffer lessonOffer) {
		return LessonOfferResource.getLessonOffersWithFilters(lessonOffer);
	}		
	
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteLessonOffer(LessonOffer lessonOffer) {
		LessonOfferResource.deleteLessonOffer(lessonOffer);
	}		
	
	@DELETE
	@Path("/{lessonOfferId}")
	public void deleteLessonOfferById(int lessonOfferId) {
		LessonOfferResource.deleteLessonOfferById(lessonOfferId);
	}		
	
	
}
