package ee.lessons.privatelessons.controller;

import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ee.lessons.privatelessons.dao.Address;
import ee.lessons.privatelessons.resource.AddressResource;



@Path("/addresses")
public class AddressController {
	@GET
	@Produces (MediaType.APPLICATION_JSON) 
	public Set<Address> getAllAddresses() {
		return AddressResource.getAllAddresses();
	}

	@GET
	@Path("/{addressId}")
	@Produces (MediaType.APPLICATION_JSON)
	public Address getAdddressesByAddressId(@PathParam("addressId") int addressId) {
		return AddressResource.getAddressById(addressId);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)		
	@Consumes(MediaType.APPLICATION_JSON)		
	public Address addNewAddress(Address address) {
		return AddressResource.addAddress(address);
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void putAddress(Address address) {
		AddressResource.updateAddressBy(address);
	}
	
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteAddress(Address address) {
		AddressResource.deleteAddressByID(address);
	}	
}
