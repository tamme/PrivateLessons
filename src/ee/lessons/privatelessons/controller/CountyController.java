package ee.lessons.privatelessons.controller;

import java.util.Set;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ee.lessons.privatelessons.dao.County;
import ee.lessons.privatelessons.resource.CountyResource;


@Path("/counties")
public class CountyController {
	@GET
	@Produces (MediaType.APPLICATION_JSON)
	public Set<County> getAllCounties() {
		return CountyResource.getAllCounties();
	}
	

	@GET
	@Path("/{countyID}")
	@Produces (MediaType.APPLICATION_JSON)
	public County getCountyById(@PathParam("countyID") int countyID) {
		return CountyResource.getcountyById(countyID);
	}
}


