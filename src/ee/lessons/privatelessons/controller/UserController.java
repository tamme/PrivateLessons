package ee.lessons.privatelessons.controller;

import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ee.lessons.privatelessons.dao.User;
import ee.lessons.privatelessons.resource.UserResource;

@Path("/users")
public class UserController {

	@GET
	@Produces (MediaType.APPLICATION_JSON) // see meetod toodab jsonit, väljastatakse json kujul
	public Set<User> getAllUsers() {
		return UserResource.getAllUsers();
	}

	@GET
	@Path("/{UserId}")
	@Produces (MediaType.APPLICATION_JSON)
	public User getAdddressesByUserId(@PathParam("UserId") int UserId) {
		return UserResource.getUserById(UserId);
	}
	
	@GET
	@Path("/username/{userName}")
	@Produces (MediaType.APPLICATION_JSON)
	public User getUserName(@PathParam("userName") String userName) {
		return UserResource.getUserByUsername(userName);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)		
	@Consumes(MediaType.APPLICATION_JSON)		
	public User addNewUser(User User) {
		return UserResource.addUser(User);
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void putUser(User User) {
		UserResource.updateUserBy(User);
	}
}
