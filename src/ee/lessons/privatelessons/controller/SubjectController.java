package ee.lessons.privatelessons.controller;

import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.lessons.privatelessons.dao.Subject;
import ee.lessons.privatelessons.resource.SubjectResource;

@Path("/subjects")

public class SubjectController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Set<Subject> getAllsubjects() {
		return SubjectResource.getAllSubjects();

	}
}
