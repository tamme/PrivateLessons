package ee.lessons.privatelessons.controller;

import java.util.Set;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ee.lessons.privatelessons.dao.City;
import ee.lessons.privatelessons.dao.County;
import ee.lessons.privatelessons.dao.Parish;
import ee.lessons.privatelessons.resource.CityResource;
import ee.lessons.privatelessons.resource.CountyResource;
import ee.lessons.privatelessons.resource.ParishResource;


@Path("/cities")
public class CityController {
	@GET
	@Produces (MediaType.APPLICATION_JSON)
	public Set<City> getAllCities() {
		return CityResource.getAllCities();
	}
	
	

	@GET
	@Path("/{countyId}")
	@Produces (MediaType.APPLICATION_JSON)
	public Set<City>  getCityByCounty(@PathParam("countyId") int countyID) {
		return CityResource.getCityByCounty(countyID);
	}

	@GET
	@Path("/byid/{cityID}")
	@Produces (MediaType.APPLICATION_JSON)
	public City getCityById(@PathParam("cityID") int cityID) {
		return CityResource.getCityById(cityID);
	}	
	
	
}
