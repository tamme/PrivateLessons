package ee.lessons.privatelessons.dao;

import java.math.BigDecimal;
import java.util.Date;

public class LessonOffer {
	private int id;
	private int subjectId;
	private String description;
	private int addressId;
	private int distance;
	private int groupsize;
	private BigDecimal price;
	private int minutes;
	private int userId;
	private int wish;
	private String subjectName;
	private String countyName;
	private int countyId;
	private String parishName;
	private String cityName;
	private String street;
	private String firstName;
	private String lastName;
	private Date birthDay;
	private int phone;
	private String email;
	private String userName;
	private BigDecimal lat;
	private BigDecimal lng;
	private BigDecimal startpointLat;
	private BigDecimal startpointLng;
	private int age;
	private int km;
	
	
	@Override
	public String toString() {
		return "LessonOffer [id = " + id 
				+ ", subjectId = " + subjectId 
				+ ", description = " + description
				+ ", addressId = " + addressId
				+ ", distance = " + distance
				+ ", groupsize = " + groupsize
				+ ", price = " + price 
				+ ", minutes = " + minutes
				+ ", userId = " + userId
				+ ", age = " + age
				+ ", km = " + km
				+ ", lat = " + lat
				+ ", lng = " + lng
				+ ", startpointLat = " + startpointLat
				+ ", startpointLng = " + startpointLng
				+ ", countyId = " + countyId
				+ ", wish = " + wish;
		
	}
	
	public int getUserId() {
		return userId;
	}
	public LessonOffer setUserId(int userId) {
		this.userId = userId;
		return this;
	}
	public int getId() {
		return id;
	}
	public LessonOffer setId(int id) {
		this.id = id;
		return this;
	}
	public int getSubjectId() {
		return subjectId;
	}
	public LessonOffer setSubjectId(int subjectid) {
		this.subjectId = subjectid;
		return this;
	}
	public String getDescription() {
		return description;
	}
	public LessonOffer setDescription(String description) {
		this.description = description;
		return this;
	}
	public int getAddressId() {
		return addressId;
	}
	public LessonOffer setAddressId(int addressid) {
		this.addressId = addressid;
		return this;
	}
	public int getDistance() {
		return distance;
	}
	public LessonOffer setDistance(int distance) {
		this.distance = distance;
		return this;
	}
	public int getGroupsize() {
		return groupsize;
	}
	public LessonOffer setGroupsize(int groupsize) {
		this.groupsize = groupsize;
		return this;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public LessonOffer setPrice(BigDecimal price) {
		this.price = price;
		return this;
	}
	public int getMinutes() {
		return minutes;
	}
	public LessonOffer setMinutes(int minutes) {
		this.minutes = minutes;
		return this;
	}

	public int getWish() {
		return wish;
	}

	public LessonOffer setWish(int wish) {
		this.wish = wish;
		return this;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public LessonOffer setSubjectName(String subjectName) {
		this.subjectName = subjectName;
		return this;
	}

	public String getCountyName() {
		return countyName;
	}

	public LessonOffer setCountyName(String countyName) {
		this.countyName = countyName;
		return this;
	}

	public String getParishName() {
		return parishName;
	}

	public LessonOffer setParishName(String parishName) {
		this.parishName = parishName;
		return this;
	}

	public String getFirstName() {
		return firstName;
	}

	public LessonOffer setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public int getKm() {
		return km;
	}

	public LessonOffer setKm(int km) {
		this.km = km;
		return this;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public LessonOffer setLat(BigDecimal lat) {
		this.lat = lat;
		return this;
	}

	public LessonOffer setLng(BigDecimal lng) {
		this.lng = lng;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public LessonOffer setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public Date getBirthDay() {
		return birthDay;
	}

	public LessonOffer setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
		return this;
	}

	public int getPhone() {
		return phone;
	}

	public LessonOffer setPhone(int phone) {
		this.phone = phone;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public LessonOffer setEmail(String email) {
		this.email = email;
		return this;
	}

	public String getUserName() {
		return userName;
	}

	public int getAge() {
		return age;
	}

	public LessonOffer setAge(int age) {
		this.age = age;
		return this;
	}

	public LessonOffer setUserName(String userName) {
		this.userName = userName;
		return this;
	}

	public BigDecimal getStartpointLat() {
		return startpointLat;
	}

	public BigDecimal getStartpointLng() {
		return startpointLng;
	}

	public LessonOffer setStartpointLat(BigDecimal startpointLat) {
		this.startpointLat = startpointLat;
		return this;
	}

	public LessonOffer setStartpointLng(BigDecimal startpointLng) {
		this.startpointLng = startpointLng;
		return this;
	}

	public int getCountyId() {
		return countyId;
	}

	public LessonOffer setCountyId(int countyId) {
		this.countyId = countyId;
		return this;
	}

	public String getCityName() {
		return cityName;
	}

	public String getStreet() {
		return street;
	}

	public LessonOffer setCityName(String cityName) {
		this.cityName = cityName;
		return this;
	}

	public LessonOffer setStreet(String street) {
		this.street = street;
		return this;
	}

}
