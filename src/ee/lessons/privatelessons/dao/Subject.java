package ee.lessons.privatelessons.dao;

public class Subject {
	private int id;
	private String name;
	
	
	@Override
	public String toString() {
		return "Subject [id = " + id 
				+ ", name = " + name;
	}
	
	public int getId() {
		return id;
	}
	public Subject setId(int id) {
		this.id = id;
		return this;
	}
	public String getName() {
		return name;
	}
	public Subject setName(String name) {
		this.name = name;
		return this;
	}

}
