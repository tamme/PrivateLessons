package ee.lessons.privatelessons.dao;

public class City {
	private int id;
	private String name;
	private int county_id;
	private int parish_id;
	
	
	@Override
	public String toString() {
		return "City [id = " + id 
				+ ", name = " + name 
				+ ", county_id = " + county_id
				+ ", parish_id = " + parish_id;
	}
	
	
	public int getId() {
		return id;
	}
	public City setId(int id) {
		this.id = id;
		return this;
	}
	public String getName() {
		return name;
	}
	public City setName(String name) {
		this.name = name;
		return this;
	}
	public int getCounty_id() {
		return county_id;
	}
	public City setCounty_id(int county_id) {
		this.county_id = county_id;
		return this;
	}
	public int getParish_id() {
		return parish_id;
	}
	public City setParish_id(int parish_id) {
		this.parish_id = parish_id;
		return this;
	}
}
