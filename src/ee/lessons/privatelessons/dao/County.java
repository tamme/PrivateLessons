package ee.lessons.privatelessons.dao;

public class County {
	private int id;
	private String name;
	
	
	@Override
	public String toString() {
		return "County [id = " + id 
				+ ", name = " + name;
	}
	
	public int getId() {
		return id;
	}
	public County setId(int id) {
		this.id = id;
		return this;
	}
	public String getName() {
		return name;
	}
	public County setName(String name) {
		this.name = name;
		return this;
	}
}
