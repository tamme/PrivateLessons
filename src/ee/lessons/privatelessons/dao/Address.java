package ee.lessons.privatelessons.dao;

import java.math.BigDecimal;

public class Address {
	
	private int id;
	private int cityId;
	private int countyId;
	private int parishId;
	private String street;
	private String cityName;
	private String countyName;
	private String parishName;
	private BigDecimal lat;
	private BigDecimal lng;

	@Override
	public String toString() {
		return "Address[id = " + id 
				+ ", cityid = " + cityId 
				+ ", countyid = " + countyId
				+ ", parishid = " + parishId
				+ ", street = " + street
				+ ", cityName = " + cityName
				+ ", countyName = " + countyName
				+ ", lat = " + lat
				+ ", lng = " + lng
				+ ", parishName = " + parishName;
	}
	
	
	public int getId() {
		return id;
	}
	public Address setId(int id) {
		this.id = id;
		return this;
	}
	public int getCityId() {
		return cityId;
	}
	public Address setCityId(int cityid) {
		this.cityId = cityid;
		return this;
	}
	public int getCountyId() {
		return countyId;
	}
	public Address setCountyId(int countyid) {
		this.countyId = countyid;
		return this;
	}
	public int getParishId() {
		return parishId;
	}
	public Address setParishId(int parishid) {
		this.parishId = parishid;
		return this;
	}
	public String getStreet() {
		return street;
	}
	public Address setStreet(String street) {
		if (street == null) street = "";
		this.street = street;
		return this;
	}
	public String getCityName() {
		return cityName;
	}
	public Address setCityName(String cityName) {
		if (cityName == null) cityName = "";
		this.cityName = cityName;
		return this;
	}
	public String getCountyName() {
		return countyName;
	}
	public Address setCountyName(String countyName) {
		if (countyName == null) countyName = "";
		this.countyName = countyName;
		return this;
	}
	public String getParishName() {
		return parishName;
	}
	public Address setParishName(String parishName) {
		if (parishName == null) parishName = "";
		this.parishName = parishName;
		return this;
	}
	public BigDecimal getLat() {
		return lat;
	}
	public Address setLat(BigDecimal lat) {
		this.lat = lat;
		return this;
	}
	public BigDecimal getLng() {
		return lng;
	}
	public Address setLng(BigDecimal lng) {
		this.lng = lng;
		return this;
	}
}
