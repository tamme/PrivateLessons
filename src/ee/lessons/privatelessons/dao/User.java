package ee.lessons.privatelessons.dao;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class User {
	private int id;
	private String firstname;
	private String lastname;
	private Date birthday;
	private int addressId;
	private int phone;
	private String email;
	private String username;
	private String password;
	private String birthdayString;
	
	@Override
	public String toString() {
		return "User[id = " + id 
				+ ", firstname = " + firstname 
				+ ", lastname = " + lastname
				+ ", birthday = " + birthday
				+ ", address_id = " + addressId
				+ ", phone = " + phone
				+ ", email = " + email
				+ ", username = " + username
				+ ", password = " + password;
	}
	
	
	public int getId() {
		return id;
	}
	public User setId(int id) {
		this.id = id;
		return this;
	}
	public String getFirstname() {
		return firstname;
	}
	public User setFirstname(String firstname) {
		this.firstname = firstname;
		return this;
	}
	public String getLastname() {
		return lastname;
	}
	public User setLastname(String lastname) {
		this.lastname = lastname;
		return this;
	}
	public Date getBirthday() {
		return birthday;
	}
	public User setBirthday(Date birthday) {
		this.birthday = birthday;
		return this;
	}
	public int getAddressId() {
		return addressId;
	}
	public User setAddressId(int addressid) {
		this.addressId = addressid;
		return this;
	}
	public int getPhone() {
		return phone;
	}
	public User setPhone(int phone) {
		this.phone = phone;
		return this;
	}

	public String getEmail() {
		return email;
	}
	public User setEmail(String email) {
		this.email = email;
		return this;
	}
	public String getUsername() {
		return username;
	}
	public User setUsername(String username) {
		this.username = username;
		return this;
	}
	public String getPassword() {
		return password;
	}
	public User setPassword(String password) {
		this.password = password;
		return this;
	}

	public String getBirthdayString() {
		return birthdayString;
	}



	public User setBirthdayString(String birthdayString) {
		this.birthdayString = birthdayString;
		try {
			this.setBirthday(new SimpleDateFormat("dd-MM-yyyy").parse(this.getBirthdayString()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return this;
	}
}
