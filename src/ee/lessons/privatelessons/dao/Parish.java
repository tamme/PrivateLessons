package ee.lessons.privatelessons.dao;

public class Parish {
	private int id;
	private String name;
	private int county_id;

	@Override
	public String toString() {
		return "Parish [id = " + id + ", name = " + name + ", county_id = " + county_id;
	}

	public int getId() {
		return id;
	}

	public Parish setId(int id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public Parish setName(String name) {
		this.name = name;
		return this;
	}

	public int getCounty_id() {
		return county_id;
	}

	public Parish setCounty_id(int county_id) {
		this.county_id = county_id;
		return this;
	}

}
