-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: private_lessons
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `city_id` int(2) DEFAULT NULL,
  `county_id` int(2) NOT NULL,
  `parish_id` int(2) DEFAULT NULL,
  `street` varchar(45) DEFAULT NULL,
  `lat` decimal(10,6) DEFAULT NULL,
  `lng` decimal(10,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=372 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (365,4,1,3,'Koondise 7-15',59.324145,24.557206),(366,0,1,7,'',NULL,NULL),(367,6,15,14,'Sireli 4-18',58.994404,22.748232),(368,27,4,0,'Vee 15-2',NULL,NULL),(369,6,15,14,'Sireli 4-6',58.994404,22.748232),(370,0,1,5,'Liikva Otsa 12',59.386406,24.577042),(371,33,2,0,'Soola 8',58.378602,26.731024);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `county_id` int(2) NOT NULL,
  `parish_id` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Keila',1,NULL),(2,'Loksa',1,NULL),(3,'Maardu',1,NULL),(4,'Saue',1,3),(5,'Tallinn',1,NULL),(6,'Kärdla',15,14),(7,'Jõhvi',3,16),(8,'Kohtla-Järve',3,NULL),(9,'Kiviõli',3,17),(10,'Püssi',3,17),(11,'Narva',3,NULL),(12,'Narva-Jõesoo',3,NULL),(13,'Sillamäe',3,NULL),(14,'Jõgeva',10,19),(15,'Mustvee',10,20),(16,'Põltsamaa',10,21),(17,'Paide',11,NULL),(18,'Türi',11,23),(19,'Haapsalu',14,NULL),(20,'Rakvere',5,NULL),(21,'Tamsalu',5,29),(22,'Tapa',5,29),(23,'Kunda',5,31),(24,'Põlva',13,34),(25,'Räpina',13,35),(26,'Lihula',4,38),(27,'Pärnu',4,NULL),(28,'Kilingi-Nõmme',4,40),(29,'Rapla',7,45),(30,'Kuressaare',9,48),(31,'Elva',2,49),(32,'Kallaste',2,54),(33,'Tartu',2,NULL),(34,'Otepää',12,56),(35,'Tõrva',12,57),(36,'Valga',12,58),(37,'Abja-Paluoja',6,59),(38,'Karksi-Nuia',6,59),(39,'Mõisaküla',6,59),(40,'Suure-Jaani',6,60),(41,'Võhma',6,60),(42,'Viljandi',6,NULL),(43,'Antsla',8,62),(44,'Võru',8,NULL),(45,'Kehra',1,10),(48,'Paldiski',1,1);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `county`
--

DROP TABLE IF EXISTS `county`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `county` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `county`
--

LOCK TABLES `county` WRITE;
/*!40000 ALTER TABLE `county` DISABLE KEYS */;
INSERT INTO `county` VALUES (1,'Harju'),(2,'Tartu'),(3,'Ida-Viru'),(4,'Pärnu'),(5,'Lääne-Viru'),(6,'Viljandi'),(7,'Rapla'),(8,'Võru'),(9,'Saare'),(10,'Jõgeva'),(11,'Järva'),(12,'Valga'),(13,'Põlva'),(14,'Lääne'),(15,'Hiiu');
/*!40000 ALTER TABLE `county` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lesson_offer`
--

DROP TABLE IF EXISTS `lesson_offer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lesson_offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` int(3) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `address_id` int(6) DEFAULT NULL,
  `distance` int(3) DEFAULT NULL,
  `groupsize` int(3) DEFAULT '1',
  `price` decimal(7,2) DEFAULT NULL,
  `minutes` int(3) DEFAULT '45',
  `user_id` int(11) NOT NULL,
  `wish` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lesson_offer`
--

LOCK TABLES `lesson_offer` WRITE;
/*!40000 ALTER TABLE `lesson_offer` DISABLE KEYS */;
INSERT INTO `lesson_offer` VALUES (104,1,'Differentsiaalvõrrandid',365,10,1,20.00,45,54,0),(105,6,'Otsin klassikalise kitarri õpetajat',366,30,0,20.00,0,53,1),(106,3,'Biokeemia',369,5,0,15.00,0,57,1),(107,11,'',371,5,1,100.00,45,54,0),(108,3,'Orgaaniline',370,5,0,31.00,0,69,1);
/*!40000 ALTER TABLE `lesson_offer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `offers`
--

DROP TABLE IF EXISTS `offers`;
/*!50001 DROP VIEW IF EXISTS `offers`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `offers` AS SELECT 
 1 AS `subjectName`,
 1 AS `countyName`,
 1 AS `parishName`,
 1 AS `cityName`,
 1 AS `firstname`,
 1 AS `lastname`,
 1 AS `birthday`,
 1 AS `age`,
 1 AS `phone`,
 1 AS `email`,
 1 AS `username`,
 1 AS `subject_id`,
 1 AS `id`,
 1 AS `description`,
 1 AS `address_id`,
 1 AS `distance`,
 1 AS `price`,
 1 AS `groupsize`,
 1 AS `minutes`,
 1 AS `user_id`,
 1 AS `lat`,
 1 AS `lng`,
 1 AS `wish`,
 1 AS `county_id`,
 1 AS `street`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `parish`
--

DROP TABLE IF EXISTS `parish`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parish` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `county_id` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parish`
--

LOCK TABLES `parish` WRITE;
/*!40000 ALTER TABLE `parish` DISABLE KEYS */;
INSERT INTO `parish` VALUES (1,'Lääne-Harju',1),(2,'Keila',1),(3,'Saue',1),(4,'Saku',1),(5,'Harku',1),(6,'Kiili',1),(7,'Rae',1),(8,'Raasiku',1),(9,'Kose',1),(10,'Anija',1),(11,'Kuusalu',1),(12,'Viimsi',1),(13,'Jõelähtme',1),(14,'Hiiumaa',15),(15,'Alutaguse',3),(16,'Jõhvi',3),(17,'Lüganuse',3),(18,'Toila',3),(19,'Jõgeva',10),(20,'Mustvee',10),(21,'Põltsamaa',10),(22,'Järva',11),(23,'Türi',11),(24,'Lääne-Nigula',14),(25,'Vormsi',14),(26,'Haljala',5),(27,'Kadrina',5),(28,'Rakvere',5),(29,'Tapa',5),(30,'Vinni',5),(31,'Viru-Nigula',5),(32,'Väike-Maarja',5),(33,'Kanepi',13),(34,'Põlva',13),(35,'Räpina',13),(36,'Häädemeeste',4),(37,'Kihnu',4),(38,'Lääneranna',4),(39,'Põhja-Pärnumaa',4),(40,'Saarde',4),(41,'Tori',4),(42,'Kehtna',7),(43,'Kohila',7),(44,'Märjamaa',7),(45,'Rapla',7),(46,'Muhu',9),(47,'Ruhnu',9),(48,'Saaremaa',9),(49,'Elva',2),(50,'Kambja',2),(51,'Kastre',2),(52,'Luunja',2),(53,'Nõo',2),(54,'Peipsiääre',2),(55,'Tartu',2),(56,'Otepää',12),(57,'Tõrva',12),(58,'Valga',12),(59,'Mulgi',6),(60,'Põhja-Sakala',6),(61,'Viljandi',6),(62,'Antsla',8),(63,'Rõuge',8),(64,'Setomaa',8),(65,'Võru',8);
/*!40000 ALTER TABLE `parish` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rate`
--

DROP TABLE IF EXISTS `rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rater` int(9) DEFAULT NULL,
  `rating` tinyint(4) DEFAULT NULL,
  `teacher` int(9) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rate`
--

LOCK TABLES `rate` WRITE;
/*!40000 ALTER TABLE `rate` DISABLE KEYS */;
/*!40000 ALTER TABLE `rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject`
--

LOCK TABLES `subject` WRITE;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
INSERT INTO `subject` VALUES (1,'Math'),(2,'Physics'),(3,'Chemistry'),(4,'Russian'),(5,'English'),(6,'Guitar'),(7,'Piano'),(8,'Drums'),(9,'Driving'),(10,'Surf'),(11,'Personal trainer'),(12,'History');
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(25) DEFAULT '',
  `lastname` varchar(25) DEFAULT '',
  `birthday` date DEFAULT NULL,
  `address_id` int(6) unsigned DEFAULT NULL,
  `phone` int(10) unsigned DEFAULT '0',
  `email` varchar(45) DEFAULT '',
  `username` varchar(25) NOT NULL DEFAULT '',
  `password` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (53,'Kaspar','Tall','1987-03-18',0,55123654,'kaspar@gmail.com','kaspar','123456'),(54,'Helen','Põllu','1970-08-08',0,55456254,'helen.pollu@gmail.com','helen','123456'),(56,'Tiit','Karu','1950-11-08',0,5465787,'tiitkaru@gmail.com','Tiit','123456'),(57,'Peeter','Peri','1980-11-07',369,55147854,'peeter.peri@gmail.com','Peeter','123456'),(58,'Katja','Põder','1960-11-15',0,6348754,'-','Katja','123456'),(59,'Tõnis','Vaikmaa','1980-11-14',0,55654895,'tonis@mail.ee','tonis','123456'),(61,'Raul','Raud','1999-11-23',0,55987374,'raul@gmail.com','Raul','123456'),(62,'Tarmo','Taak','2001-11-15',0,5546765,'tarmo@gmail.com','Tarmo','123456'),(63,'Heino','Leonard','1985-11-15',0,6457879,'leo@pold.ee','Leonard','123456'),(64,'Teodor','Kass','1945-11-16',0,5487898,'sussvervusser@gmail.com','Teodor','123456'),(66,'Katrin','Tamm','1974-06-14',0,55634345,'katrin@gmail.com','katu','123456'),(67,'Vladislav','Rubin','1965-08-15',0,52347654,'ruudi@hot.ee','vlad','123456'),(68,'Sten','Ork','1995-11-10',0,5412578,'sten@kork.ee','Sten','123456'),(69,'Mehis','Kadakas','1981-11-07',370,5465487,'mehike@gmail.com','Mehis','123456');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `offers`
--

/*!50001 DROP VIEW IF EXISTS `offers`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `offers` AS select `subject`.`name` AS `subjectName`,`county`.`name` AS `countyName`,`parish`.`name` AS `parishName`,`city`.`name` AS `cityName`,`user`.`firstname` AS `firstname`,`user`.`lastname` AS `lastname`,`user`.`birthday` AS `birthday`,(date_format(from_days((to_days(now()) - to_days(`user`.`birthday`))),'%Y') + 0) AS `age`,`user`.`phone` AS `phone`,`user`.`email` AS `email`,`user`.`username` AS `username`,`lesson_offer`.`subject_id` AS `subject_id`,`lesson_offer`.`id` AS `id`,`lesson_offer`.`description` AS `description`,`lesson_offer`.`address_id` AS `address_id`,`lesson_offer`.`distance` AS `distance`,`lesson_offer`.`price` AS `price`,`lesson_offer`.`groupsize` AS `groupsize`,`lesson_offer`.`minutes` AS `minutes`,`lesson_offer`.`user_id` AS `user_id`,`address`.`lat` AS `lat`,`address`.`lng` AS `lng`,`lesson_offer`.`wish` AS `wish`,`county`.`id` AS `county_id`,`address`.`street` AS `street` from ((((((`lesson_offer` left join `subject` on((`lesson_offer`.`subject_id` = `subject`.`id`))) left join `address` on((`lesson_offer`.`address_id` = `address`.`id`))) left join `county` on((`address`.`county_id` = `county`.`id`))) left join `parish` on((`address`.`parish_id` = `parish`.`id`))) left join `city` on((`address`.`city_id` = `city`.`id`))) left join `user` on((`lesson_offer`.`user_id` = `user`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-29 16:37:40
