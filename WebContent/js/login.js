
function userlogin(){

	if (userID > 0){
		var selectorContent = '<a href="#" onClick = "logout();"><span class="glyphicon glyphicon-log-out" id="login"></span> Logout</a>';
		document.getElementById("login").innerHTML = selectorContent;
		

		var content = "<a href = '#' onClick = 'userArea();'>" + userinfo.username + "</a>";
		document.getElementById("userName").innerHTML = content;
	}
	else {
		  var selectorContent = '<a href="#" onClick = "login();"><span class="glyphicon glyphicon-log-in" id="login"></span> Login</a>';
		//var selectorContent = "<a href='#' id='login' onClick = 'login()'>Login</a>";
		document.getElementById("login").innerHTML = selectorContent;
	}
}


function login() {
	content = `		<h1>User login</h1>
<div class="container">
	<form name='userlogin' class="form-horizontal">

		<div class="form-group">
			<label class="control-label col-sm-2" for="Username">Username:</label>
			<div class="col-sm-3">
				<input type="text" class="form-control" id="loginname">
			</div>
		</div>		
		
		<div class="form-group">
			<label class="control-label col-sm-2" for="Password">Password:</label>
			<div class="col-sm-3">
	        	<input type="password" class="form-control" id="loginpassword">
	        </div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for="update"></label>        
				<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default" onClick="checkPassword();">OK</button>
				</div>
		</div>	
	</form>
</div>`;
		document.getElementById("userlogin").innerHTML = content;
}


function signUp() {
	content = `<h1>New user registration</h1>
	<div class="container">
		<form id="registration" class="form-horizontal">
		
		<div class="form-group">
			<label class="control-label col-sm-2" for="Username">Username:</label>
			<div class="col-sm-3">
				<input type="text" class="form-control" id="username">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-sm-2" for="Password">Password:</label>
			<div class="col-sm-3">
	        	<input type="password" class="form-control" id="password1">
	        </div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-sm-2" for="Password">Password:</label>
			<div class="col-sm-3">
	        	<input type="password" class="form-control" id="password2">
	        </div>
		</div>
					
		<div class="form-group">
			<label class="control-label col-sm-2" for="Firstname">Firstname:</label>
			<div class="col-sm-3">
	        	<input type="text" class="form-control" id="firstname">
	        </div>
		</div>
				
		<div class="form-group">
			<label class="control-label col-sm-2" for="Lastname">Lastname:</label>
			<div class="col-sm-3">
	        	<input type="text" class="form-control" id="lastname">
	        </div>
		</div>		
			
 
		<div class="form-group">
			<label class="control-label col-sm-2" for="Birthday">Birthday:</label>
			<div class="col-sm-3">
	        	<input type="date" class="form-control" id="birthday">
	        </div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-sm-2" for="Phone">Phone:</label>
			<div class="col-sm-3">
	        	<input type="tel" class="form-control" id="phone">
	        </div>
		</div>
				
		<div class="form-group">
			<label class="control-label col-sm-2" for="e-mail">e-mail:</label>
			<div class="col-sm-3">
	        	<input type="email" class="form-control" id="email">
	        </div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for="update"></label>        
				<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default" onClick="addNewUser();">Save and continue</button>
				</div>
		</div>		
			
		</form>
	</div>	`;
		document.getElementById("userlogin").innerHTML = content;
}


function logout(){
	userID = null;
	localStorage.setItem("userID", userID);
	
	window.location.replace("http://127.0.0.1:8080/PrivateLessons");
}

function checkPassword() {
	var userName = document.getElementById("loginname").value;
	var password = document.getElementById("loginpassword").value;

	$.getJSON("http://127.0.0.1:8080/PrivateLessons/rest/users/username/" + userName, function(userdata) {
	if (userdata) {
		if (password == userdata.password)
		{
		userID = userdata.id;
		
		localStorage.setItem("userID", userID);
		
		
		content = "<p>Login succcess! Logged in as " + userdata.username + "</p>";
		document.getElementById("userlogin").innerHTML = content;
		window.location.replace("http://127.0.0.1:8080/PrivateLessons");
		}
	else
		{
		alert("Wrong password!")
		}
	}
	else {
		alert("Wrong username!")
	}
});
}
	

