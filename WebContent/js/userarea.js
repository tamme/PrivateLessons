

function userArea(){
	content = `<h1>My contacts</h1>
	<div class="container">
	<form id="userInfoUpdate" class="form-horizontal">
	
	<div class="form-group">
		<label class="control-label col-sm-2" for="Username">Username:</label>
		<div class="col-sm-3">
        	<input type="text" class="form-control" id="username">
        </div>
	</div>
	
	<div class="form-group">
		<label class="control-label col-sm-2" for="Password">Password:</label>
		<div class="col-sm-3">
        	<input type="text" class="form-control" id="password">
        </div>
	</div>
			
	<div class="form-group">
		<label class="control-label col-sm-2" for="Firstname">Firstname:</label>
		<div class="col-sm-3">
        	<input type="text" class="form-control" id="firstname">
        </div>
	</div>
			
	<div class="form-group">
		<label class="control-label col-sm-2" for="Lastname">Lastname:</label>
		<div class="col-sm-3">
        	<input type="text" class="form-control" id="lastname">
        </div>
	</div>
			
	<div class="form-group">
		<label class="control-label col-sm-2" for="Birthday">Birthday:</label>
		<div class="col-sm-3">
        	<input type="date" class="form-control" id="birthday">
        </div>
	</div>
	
	<div class="form-group">
		<label class="control-label col-sm-2" for="Phone">Phone:</label>
		<div class="col-sm-3">
        	<input type="tel" class="form-control" id="phone">
        </div>
	</div>
			
	<div class="form-group">
		<label class="control-label col-sm-2" for="e-mail">e-mail:</label>
		<div class="col-sm-3">
        	<input type="email" class="form-control" id="email">
        </div>
	</div>
						
		<div class="form-group">
			<label class="control-label col-sm-2" for="update"></label>        
				<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default" onClick="updateUserData();">Update</button>
				</div>
		</div>			
</form>

</div>
<div id = "address"></div>
<div id = "adds"></div>`;
	
	document.getElementById("userlogin").innerHTML = content;
	document.getElementById("username").value = userinfo.username;
	document.getElementById("password").value = userinfo.password;
	document.getElementById("firstname").value = userinfo.firstname;
	document.getElementById("lastname").value = userinfo.lastname;
	document.getElementById("birthday").value = userinfo.birthday.split("T")[0];
	document.getElementById("phone").value = userinfo.phone;
	document.getElementById("email").value = userinfo.email;
	showUserAdds();
	addressForm("address","",addressinfo.countyId, addressinfo.parishId, addressinfo.cityId, addressinfo.street, addressinfo.id);
	console.log("user address" + JSON.stringify(addressinfo));
}

function updateUserData() {
	var modifyUserJson = {
			"id": userinfo.id,
			"addressId": userinfo.addressId,
			"username" : document.getElementById("username").value,
			"password" : document.getElementById("password").value,
			"firstname" : document.getElementById("firstname").value,
			"lastname" : document.getElementById("lastname").value,
			"birthday" : document.getElementById("birthday").value + "T00:00:00+02:00",
			"phone" : document.getElementById("phone").value,
			"email" : document.getElementById("email").value
	};
	console.log(modifyUserJson);
	var modifyUser = JSON.stringify(modifyUserJson);
	
	$.ajax({
		url : "http://127.0.0.1:8080/PrivateLessons/rest/users",
		type: "PUT",
		data: modifyUser, 
		contentType: "application/json; charset=utf-8",
		success: function (){
			console.log("saadetud string " + JSON.stringify(modifyUserJson));
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
			console.log(modifyUserJson);
		}
	});
}

function showUserAdds() {
var filters = {
	"userId" : userinfo.id,	
	"wish" : 2
	};

console.log(JSON.stringify(filters));
	$.ajax({
        url: "http://127.0.0.1:8080/PrivateLessons/rest/lessons/filters",
        type: "POST",
        data: JSON.stringify(filters),
        contentType: "application/json; charset=utf-8",
        success: function (offers) {
            var coordinates = "";
            var tableBody = document.getElementById("addressTableBody");
            var headline;
            var firstAddsColumn = `<div class="col-sm-4" >`;
            var secondAddsColumn = `<div class="col-sm-4" >`;
            var thirdAddsColumn = `<div class="col-sm-4" >`;
			
		
            for (var i = 0; i < offers.length; i++) {
            	if (offers[i].wish == 0) {headline = "My offer: ";}
            	else {headline = "My wish: ";}
            	
            	if (offers[i].parishName == null) {offers[i].parishName = "";}
            	if (offers[i].cityName == null) {offers[i].cityName = "";}
            	if (offers[i].countyName == null) {offers[i].countyName = "";}
            	if (offers[i].street == null) {offers[i].street = "";}
            	if (offers[i].price == null) {offers[i].price = "";}
            	if (offers[i].minutes == null || offers[i].minutes == 0  ) {offers[i].minutes = "";}
            	
               	var addColumn =  `<div class = "container addsdiv" style = "width: 300px"><h3>`+ headline + offers[i].subjectName + `</h3><p>address: ` 
               	+ offers[i].countyName + " " + offers[i].parishName + " " + offers[i].cityName + " " + offers[i].street 
                + "</p><p>distance: " + offers[i].distance
                + "</p><p>price: " + offers[i].price
                + "</p><p>minutes: " + offers[i].minutes
                + "</p><div class='col-sm-offset-2 col-sm-10' style = 'position: relative; left: 80px; bottom: 50px;'><button type = 'button' class='btn btn-default' onClick = 'deleteAdd(" + offers[i].id + ");'>Delete</button></div></div>";
            
            	if (i%2 == 0) { var secondAddsColumn = secondAddsColumn + addColumn; }
            	else { var firstAddsColumn = firstAddsColumn + addColumn; }
            };
            secondAddsColumn = secondAddsColumn + "</div>";
            firstAddsColumn = firstAddsColumn + "</div>";
            thirdAddsColumn = thirdAddsColumn + "</div>";
            var addsContent = "<h1>My active adds</h1><div>" + firstAddsColumn + secondAddsColumn + thirdAddsColumn + "</div>";
            document.getElementById("adds").innerHTML = addsContent;    
        }
	}); 
}

function deleteAdd(offerId) {
	var deleteOfferJson = {"id": offerId};
	$.ajax({
		url : "http://127.0.0.1:8080/PrivateLessons/rest/lessons",
		type: "DELETE",
		data: JSON.stringify(deleteOfferJson), 
		contentType: "application/json; charset=utf-8",
		success: function (){
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
	showUserAdds();
}



