var WishTrueBoolean;
var AddLessonExtraFields;
var title;

function addLessonOrWish(choice) {
	
	if (choice == "wish") { 
		WishTrueBoolean == 1;
		AddLessonExtraFields = "";
		AddLessonExtraFields = `
			<input type = "hidden" id = "groupsize">
			<input type = "hidden" id = "minutes">
			<input type = "hidden" id = "wish" value = 1>
		`;
		title = "<h1>Add wish</h1>";
		
		}
	if (choice == "offer") { 
		WishTrueBoolean == 0; 
	AddLessonExtraFields = `
		<div class="form-group">
			<label class="control-label col-sm-2" for="groupsize">Group size: </label>
			<div class="col-sm-3">
        	<select class="form-control" id="groupsize">
        		<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
				<option value="9">9</option>
				<option value="10">10</option>
				<option value="11">more..</option>
			</select>
        	</div>
		</div>
		

				<div class="form-group">
					<label class="control-label col-sm-2" for="minutes">Lesson in minutes:</label>
					<div class="col-sm-3">
        				<input type="text" class="form-control" id="minutes" placeholder="45" value = "45">
					<input type = "hidden" id = "wish" value = 0>
					</div>
				</div>`;
	title = "<h1>Add lesson</h1>";
	}
	addLessonForm();
}


function addLessonForm() {

	if (userID > 0) {
		var countyString = addressinfo.countyName == null || addressinfo.countyName == ""  ? "" : addressinfo.countyName;
		var parishString =addressinfo.parishName == null || addressinfo.parishName == ""  ? "" : "," + addressinfo.parishName;
		var cityString = addressinfo.cityName == null || addressinfo.cityName == "" ? "" : "," + addressinfo.cityName;
		var streetString =addressinfo.street == null || addressinfo.street == ""  ? "" : "," + addressinfo.street;
		var useraddress = countyString + parishString + cityString + streetString;	
		var content = title + `
			<div class="container">
			<form class="form-horizontal">
			
				<div class="form-group">
					<label class="control-label col-sm-2" for="subjectSelect">Subject: </label>
					<div class="col-sm-3">
        				<select class="form-control" id="subjectSelect"></select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="description">Description:</label>
					<div class="col-sm-3">
        				<textarea  rows="4" cols="50" class="form-control" id="description" placeholder="Optional comment"></textarea>
					</div>
				</div>		

				<div class="form-group">
					<label class="control-label col-sm-2" for="distance">Distance: </label>
					<div class="col-sm-3">
        				<select class="form-control" id="distance">
        				<option value="5">5 km</option>
						<option value="10">10 km</option>
						<option value="15">15 km</option>
						<option value="20">20 km</option>
						<option value="25">25 km</option>
						<option value="30">30 km</option>
						<option value="35">35 km</option>
						<option value="40">40 km</option>
						<option value="45">45 km</option>
						<option value="50">50 km</option>
						<option value="55">55 km</option>
						<option value="60">60 km</option>
						<option value="65">65 km</option>
						<option value="70">70 km</option>
        				</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-2" for="price">Price in euros:</label>
					<div class="col-sm-3">
        				<input type="text" class="form-control" id="price" placeholder="00.00">
        			</div>
				</div>`
			+ AddLessonExtraFields +
				`<div class="form-group">
					<label class="control-label col-sm-2" for="price">Address:</label>
					<div class="col-sm-3">
        				<p class="form-control" id='userAddressInText' data-toggle="modal" data-target="#myModal" onClick ="addressForm('modaladdress', 'addlessonoffer', '', '', '', '', '');">` +useraddress + `</p>
        			</div>
				</div>
			
			<div class="form-group">
			<label class="control-label col-sm-2" for="price"></label>        
				<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default" onClick="addLesson()">Save</button>
				</div>
			</div>
							
			<div id="myModal" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Address</h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
        				</div>
						<div class="modal-body" id = "modaladdress">
						</div>
					</div>
				</div>
			</div>
		</form>
		</div>` ;
	
	document.getElementById("userlogin").innerHTML = content;
	getAllSubjects();
	}
	else
		{
		login();
		}
}

function userAddressInText(addressId) {
	$.getJSON("http://127.0.0.1:8080/PrivateLessons/rest/addresses/" + addressId, function(addressdata) {
		addressinfo = jQuery.extend(true, {}, addressdata);
	addressData(userinfo.addressId);
	
	var countyString = addressinfo.countyName == "" ? "" : addressinfo.countyName ;
	var parishString = addressinfo.parishName == "" ? "" :  "," + addressinfo.parishName;
	var cityString = addressinfo.cityName == "" ? "" :  "," + addressinfo.cityName;
	var streetString = addressinfo.street == "" ? "" : "," + addressinfo.street;
	var useraddress = countyString + parishString + cityString + streetString;	

	document.getElementById("userAddressInText").innerHTML = useraddress;

	});
}


function getAllSubjects() {
	$.getJSON("http://127.0.0.1:8080/PrivateLessons/rest/subjects", function(subjects){
		console.log(subjects); 
		var selectorBody = document.getElementById("subjectSelect");
		var selectorContent = "<option></option>";
		for (var i = 0; i < subjects.length; i++) {
			selectorContent = selectorContent 
			+ "<option value=" 
			+ subjects[i].id 
			+ ">"+subjects[i].name 
			+ "</option>";
		}
		console.log(selectorContent); 
		selectorBody.innerHTML = selectorContent;
	});
}


function addLesson() {
	userData();

	var newLessonJson = {
			"subjectId" : document.getElementById("subjectSelect").value,
			"description" : document.getElementById("description").value,
			"distance" : document.getElementById("distance").value,
			"price" : document.getElementById("price").value,
			"groupsize" : document.getElementById("groupsize").value,
			"minutes" : document.getElementById("minutes").value,
			"addressId" : userinfo.addressId,
			"userId" : userinfo.id,
			"wish" : document.getElementById("wish").value
		}
	var newLesson = JSON.stringify(newLessonJson);

	
	$.ajax({
		url : "http://127.0.0.1:8080/PrivateLessons/rest/lessons",
		type: "POST",
		data: newLesson, 
		contentType: "application/json; charset=utf-8",
		success: function (response){
			var userId = response.id;
			document.getElementById("userlogin").innerHTML = "Success!"
			
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
			console.log(newUserJson);
		}
	});
}

