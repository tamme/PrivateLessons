var j = 0;
var filters = {
	"subjectId" : "",
	"countyId" : "",
	"lat" : "",
	"lng" : "",
	"wish" : ""
};
var selectedSubject;
var selectedCounty;


function gpsButton(htmlelement) {
	content = `
	<button onclick="useMyDeviceGps("lat", "lng")">Use my device GPS</button>
	<p id = "lat"></p><p id = "lng"></p>`;
	document.getElementById(htmlelement).innnerHTML = content;
}


function showOfferTable() {

    var askDeviceCoordinates = new Promise(
            function (resolve, reject) {
               if (typeof startpointlat === 'undefined' || startpointlat == "") {
                   
                    navigator.geolocation.getCurrentPosition(function (location) {
                        var lat = location.coords.latitude;
                        var lng = location.coords.longitude;
                        resolve({"lat": lat, "lng": lng});
                    });

                } else {
                    resolve({"lat": startpointlat, "lng": startpointlng});
                }
            }
    );
    
    askDeviceCoordinates.then(function (gpsdata) {
    	
    	filters.lat = gpsdata.lat;
    	filters.lng = gpsdata.lng;
    	filters.wish = 0;
        console.log("saadan " + JSON.stringify(filters));
        $.ajax({
            url: "http://127.0.0.1:8080/PrivateLessons/rest/lessons/filters",
            type: "POST",
            data: JSON.stringify(filters),
            contentType: "application/json; charset=utf-8",
            success: function (offers) {
                var coordinates = "";
                var tableBody = document.getElementById("addressTableBody");
                var tableContent = `<h1>Lesson offers</h1>
				<div class= 'container mapdiv' id="mapcontainer"></div><p></p>
			
			<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%"><thead><tr>
					<th id = 'headerSubject'>Subject</th>
					<th>Description</th>
					<th id = 'headerCounty'>County</th>
					<th>Parish</th>
					<th>City</th>
					<th>Street</th>
					<th>Distance range, km</th>
					<th>Price</th>
					<th>Minutes</th>
					<th>Km</th>
					<th>Username</th>
					<th></th>
					</tr></thead>`;
                for (var i = 0; i < offers.length; i++) {

                    tableContent = tableContent
                    		+ "<tr><td>" + checkIfNullOrUndefined(offers[i].subjectName)
                    		+ "</td><td>" + checkIfNullOrUndefined(offers[i].description)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].countyName)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].parishName)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].cityName)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].street)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].distance)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].price)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].minutes)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].km)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].userName)
                            + `</td><td>
                          <img src="image/contact.png" alt="Contact" height="22" width="22" data-toggle="modal" data-target="#contactmodal"  
                            onClick = "showContacts('` 
                            + offers[i].userName +"','" 
							+ offers[i].email +"','"
							+ offers[i].phone +"','"
							+ offers[i].age +"','"
							+ offers[i].countyName +"','"
							+ offers[i].cityName +"','"
							+ offers[i].street +"','"
							+ offers[i].parishName +`');"></td></tr>`;



                    if (offers[i].lat != null && offers[i].lat != undefined && offers[i].lng != null && offers[i].lng != undefined) {
                        if (j != 0) {
                            coordinates = coordinates + ";";
                        }
                        var coordinates = coordinates + offers[i].userName + `,` + offers[i].lat + `,` + offers[i].lng + `,` + offers[i].phone;
                        j++;
                    }

                }
              
                tableContent = tableContent + `</table><div id = "note"></div>
				
		<div id="contactmodal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Address</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body" id = "contacttable">
					</div>
				</div>
			</div>
		</div>`;
                document.getElementById("userlogin").innerHTML = tableContent;
                initialize("'" + coordinates + "'");
                FilterSubjects();
                FilterCounties();
                var note = `<div id = "note">* Distance is calculated from your latitude ` +  Number((gpsdata.lat).toFixed(7)) + `, longitude ` + Number((filters.lng).toFixed(7)) + `.</div>`;
            	document.getElementById("note").innerHTML = note;
                $(document).ready(function() {
                    $('#example').DataTable();
                } );
            }
        });
    }
    );  
}



function showWishTable() {

    var askDeviceCoordinates = new Promise(
            function (resolve, reject) {            
                if (typeof startpointlat === 'undefined' || startpointlat == "") {
                    navigator.geolocation.getCurrentPosition(function (location) {
                        var lat = location.coords.latitude;
                        var lng = location.coords.longitude;
                        resolve({"lat": lat, "lng": lng});
                    });

                } else {
                    resolve({"lat": startpointlat, "lng": startpointlng});
                }
            }
    );

    
    askDeviceCoordinates.then(function (gpsdata) {
    	filters.lat = gpsdata.lat;
    	filters.lng = gpsdata.lng;
    	filters.wish = 1;
        console.log("saadan " + JSON.stringify(filters));
        $.ajax({
            url: "http://127.0.0.1:8080/PrivateLessons/rest/lessons/filters",
            type: "POST",
            data: JSON.stringify(filters),
            contentType: "application/json; charset=utf-8",
            success: function (offers) {
                var coordinates = "";
                var tableBody = document.getElementById("addressTableBody");
                var tableContent = `<h1>Lesson wishes</h1>
				<div class= 'container mapdiv' id="mapcontainer"></div><p></p>
			
			<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%"><thead><tr>
					<th id = 'headerSubject'>Subject</th>
					<th>Description</th>
					<th id = 'headerCounty'>County</th>
					<th>Parish</th>
					<th>City</th>
					<th>Street</th>
					<th>Distance range</th>
					<th>Price</th>
					<th>Age</th>
					<th>Km</th>
					<th>Username</th>
					<th></th>
					</tr></thead>`;
                for (var i = 0; i < offers.length; i++) {

                    tableContent = tableContent
                            + "<tr><td>" + checkIfNullOrUndefined(offers[i].subjectName)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].description)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].countyName)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].parishName)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].cityName)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].street)  
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].distance)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].price)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].age)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].km)
                            + "</td><td>" + checkIfNullOrUndefined(offers[i].userName)
                            + `</td><td><img src="image/contact.png" alt="Contact" height="22" width="22" data-toggle="modal" data-target="#contactmodal"  
                            onClick = "showContacts('` 
                            + offers[i].userName +"','" 
							+ offers[i].email +"','"
							+ offers[i].phone +"','"
							+ offers[i].age +"','"
							+ offers[i].countyName +"','"
							+ offers[i].cityName +"','"
							+ offers[i].street +"','"
							+ offers[i].parishName +`');"></td></tr>`;


                    if (offers[i].lat != null && offers[i].lat != undefined && offers[i].lng != null && offers[i].lng != undefined) {
                        if (j != 0) {
                            coordinates = coordinates + ";";
                        }
                        var coordinates = coordinates + offers[i].userName + `,` + offers[i].lat + `,` + offers[i].lng + `,` + offers[i].phone;
                        j++;
                    }
                }
                
                tableContent = tableContent + `</table><div id = "note"></div>
				
		<div id="contactmodal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Address</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body" id = "contacttable">
					</div>
				</div>
			</div>
		</div>`;
                document.getElementById("userlogin").innerHTML = tableContent;
                initialize("'" + coordinates + "'");
                FilterSubjects();
                FilterCounties();
                var note = `<div id = "note">* Distance is calculated from your latitude ` +  Number((gpsdata.lat).toFixed(7)) + `, longitude ` + Number((filters.lng).toFixed(7)) + `.</div>`;
            	document.getElementById("note").innerHTML = note;
                $(document).ready(function() {
                    $('#example').DataTable();
                } );
            }
        });
    }
    );  
}

	
function checkIfNullOrUndefined (value) {
	if (value != null && value != undefined && value != "undefined") {
		return value;
	} else {
		return "";
	}
}

function showContacts(user, email, phone, age, county, city, street, parish){
	email = checkIfNullOrUndefined(email)
	phone = checkIfNullOrUndefined(phone)
	county = county == null || county == "" || county == undefined  || county == "undefined" ? "": ", " + county;
	city = city == null || city == "" || city == undefined  || city == "undefined" ? "": ", " + city;
	street = street == null || street == "" || street == undefined  || street == "undefined" ? "": street;
	parish = parish == null || parish == ""  || parish == undefined  || parish == "undefined"  ? "" : ", " + parish;

	var qr = `BEGIN:VCARD
VERSION:4.0
FN:`+ user + `
TEL;,;VALUE=uri:tel:`+ phone +`
ADR;,PREF:;;`+ street +`;`+ city +`;`+ parish +`;`+ county +`;Estonia
EMAIL:`+ email +`
END:VCARD`
	qr = encodeURIComponent(qr)
	console.log(qr);
	var contacts = "<p>Username: " + user 
					+ "</p><p>tel: " + phone 
					+ "</p><p>E-mail: " + email
					+ "</p><p>Address: " + street + city + parish + county
					+ "</p><p>Age: " + age + `</p><div style = "position: absolute; right: 15px; top: 0px;">
					<img src="image/user.png" alt="User" height="100" width="100" >
					<img src="https://api.qrserver.com/v1/create-qr-code/?data=` + qr +`&amp;size=150x150" alt="" title="" />
					</div>`;
	
	document.getElementById("contacttable").innerHTML = contacts;
	
}

function FilterSubjects() {
	$.getJSON("http://127.0.0.1:8080/PrivateLessons/rest/subjects", function(subjects){
		console.log(subjects); 
		var selectorContent = "<select id = 'filterSubject' onChange = 'updateFilters();'><option value = " +  selectedSubject + ">Subject</option value = 0><option>All</option>";
		for (var i = 0; i < subjects.length; i++) {
			selectorContent = selectorContent 
			+ "<option value=" 
			+ subjects[i].id 
			+ ">"+subjects[i].name 
			+ "</option>";
		}
		selectorContent = selectorContent + "</select>"
		document.getElementById("headerSubject").innerHTML = selectorContent;
	});
}

function FilterCounties() {
	$.getJSON("http://127.0.0.1:8080/PrivateLessons/rest/counties", function(counties){
		var selectorContent = "<select id = 'filterCounty' onChange = 'updateFilters();'><option value = " + selectedCounty + ">County</option value = 0><option>All</option>";
		for (var i = 0; i < counties.length; i++) {
			selectorContent = selectorContent 
			+ "<option value=" 
			+ counties[i].id 
			+ ">"+counties[i].name 
			+ "</option>";
		}
		selectorContent = selectorContent + "</select>"
		document.getElementById("headerCounty").innerHTML = selectorContent;
	});
}

function updateFilters(){
	filters.subjectId = document.getElementById("filterSubject").value;
	filters.countyId = document.getElementById("filterCounty").value;
	selectedCounty = document.getElementById("filterCounty").value;
	selectedSubject = document.getElementById("filterSubject").value;
	
	showOfferTable();
	console.log(JSON.stringify(filters));
}
