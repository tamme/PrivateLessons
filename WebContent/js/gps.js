var lat;
var lng;

function addressToGps(json, address) {
	var gpsAddress = "";

	var parish = getParishById(json.parishId, function(parishreturn){
		gpsAddress = gpsAddress + "+" + parishreturn;
	});
	var city = getCityById(json.cityId, function(cityreturn){
		gpsAddress = gpsAddress + "+" + cityreturn + "+" + json.street;
	});	

	var county = getCountyById(json.countyId, function(countyreturn){
		gpsAddress = gpsAddress + countyreturn;

		gpsAddress = encodeURI(gpsAddress);
		$.getJSON("https://maps.googleapis.com/maps/api/geocode/json?address="+gpsAddress+"&key=AIzaSyCokuSbfZLlLLWUollW5bNDl5ZY1F4d8bM", function(gps){
		json.lat = gps.results[0].geometry.location.lat;
		json.lng = gps.results[0].geometry.location.lng;
		json.id = address;
		updateGpsAddress(json);
	});	
	});
}


function updateGpsAddress(json) {
	
	var modifyAddress = JSON.stringify(json);
		$.ajax({
		url : "http://127.0.0.1:8080/PrivateLessons/rest/addresses",
		type: "PUT",
		data: modifyAddress, 
		contentType: "application/json; charset=utf-8",
		success: function (){

		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
		}
	});
}


function useMyDeviceGps(htmlelement1, htmlelement2) {
	var x = document.getElementById(htmlelement1);
	var y = document.getElementById(htmlelement2);

	function getLocation() {
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(showPosition);
	    } else { 
	        x.innerHTML = "Geolocation is not supported by this browser.";
	    }
	}

	function showPosition(position) {
	    x.innerHTML = "Latitude: " + position.coords.latitude;
	    y.innerHTML = "Longitude: " + position.coords.longitude;
	}
}