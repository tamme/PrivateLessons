var startlat = 58.921239;
var startlng = 25.115688;
var locations;


function initialize(coordinates) {
	var locations = coordinates;
	var locations = locations.split(";");
	for (i = 0; i< locations.length; i++) {
		locations[i] = locations[i].split(",");
	}


    var myOptions = {
      center: new google.maps.LatLng(startlat, startlng),
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("mapcontainer"),
        myOptions);

    setMarkers(map,locations)
}

function setMarkers(map,locations){
	var marker, i
	for (i = 0; i < locations.length; i++){  
		var loan = locations[i][0]
		var lat = locations[i][1]
		var long = locations[i][2]
		var add =  locations[i][3]

		latlngset = new google.maps.LatLng(lat, long);
		var marker = new google.maps.Marker({  
        map: map, title: loan , position: latlngset  
        });
        map.setCenter(marker.getPosition())
        var content = "Username: " + loan +  '</h3>' + " Phone: " + add     
        var infowindow = new google.maps.InfoWindow()
        google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
        return function() {
           infowindow.setContent(content);
           infowindow.open(map,marker);
        };
    })(marker,content,infowindow)); 

  }
}

