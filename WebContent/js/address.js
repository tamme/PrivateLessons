
function getCountyById(id, callback) {
	$.getJSON("http://127.0.0.1:8080/PrivateLessons/rest/counties/" + id, function(counties){
		callback(counties.name);
	});
}

function getParishById(id, callback) {
	$.getJSON("http://127.0.0.1:8080/PrivateLessons/rest/parishes/byid/" + id, function(parishes){
		callback(parishes.name);
	});
}

function getCityById(id, callback) {
	$.getJSON("http://127.0.0.1:8080/PrivateLessons/rest/cities/byid/" + id, function(cities){
		callback(cities.name);
	});
}


function addOrUpdateAddress(parentform, addressId) {
	if (addressId == 0 || addressId == null || addressId == 'undefined') {
		addNewAddress(parentform, addressId);
	}
	else {
		updateAddress(parentform, addressId);
	}
}

function updateAddress(parentform, addressId) {
	var addressJson = {
			"id" : addressId,
	        "countyId": document.getElementById("countySelect").value,
	        "cityId": document.getElementById("citySelect").value,
	        "street": document.getElementById("streetSelect").value,
	        "parishId": document.getElementById("parishSelect").value
	    }
	
	var Json = JSON.stringify(addressJson);
	$.ajax({
		url : "http://127.0.0.1:8080/PrivateLessons/rest/addresses",
		type: "PUT",
		data: Json, 
		contentType: "application/json; charset=utf-8",
		success: function (){
			console.log("saadetud string " + JSON.stringify(Json));
			addressToGps(addressJson, addressId);
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
			console.log(Json);
		}
	});
}


function addNewAddress(parentform, addressId) {
	    var newAddressJson = {
	        "countyId": document.getElementById("countySelect").value,
	        "cityId": document.getElementById("citySelect").value,
	        "street": document.getElementById("streetSelect").value,
	        "parishId": document.getElementById("parishSelect").value
	    }
	
	    var newAddress = JSON.stringify(newAddressJson);
	    $.ajax({
	        url: "http://127.0.0.1:8080/PrivateLessons/rest/addresses",
	        type: "POST",
	        data: newAddress,
	        contentType: "application/json; charset=utf-8",
	        success: function (response) {
	        	addressId = response.id;
	            addressToGps(newAddressJson, addressId);
	            if (parentform == "addlessonoffer") {
	                userinfo.addressId = addressId;
	                userAddressInText(addressId);
	            } else {
	                getUserById(addressId);
	            }
	        },
	        error: function (XMLHttpRequest, textStatus, errorThrown) {
	        }
	    });
}


function getUserById(addressID){
	$.getJSON("http://127.0.0.1:8080/PrivateLessons/rest/users/" + userID, function(user){
		user.addressId = addressID;
		updateUserAddress(user);
	});
}

function updateUserAddress(user) {
	var newaddress = JSON.stringify(user);
	$.ajax({
		url : "http://127.0.0.1:8080/PrivateLessons/rest/users/",
		type: "PUT",
		data: newaddress, 
		contentType: "application/json; charset=utf-8",
		success: function (){
			var content = "Registration successful!";
			document.getElementById("userlogin").innerHTML = content;
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
		}
	});
}

function addressData(addressId) {
	$.getJSON("http://127.0.0.1:8080/PrivateLessons/rest/addresses/" + addressId, function(addressdata) {
		addressinfo = jQuery.extend(true, {}, addressdata);
		startpointlat = addressdata.lat;
		startpointlng = addressdata.lng;
	});
}


function addressForm(htmlelement, parentform, countyId, parishId, cityId, street, addressId) {
	content = `<div class="container"><h1>Address</h1>
		<form class="form-horizontal">

	<div class="form-group">
		<label class="control-label col-sm-2" for="County">County:</label>
		<div class="col-sm-3">
			<select id="countySelect" class="form-control" onchange="fillStreet('','','','');">
        	</select>
        </div>
	</div>			
			
	<div class="form-group" id="parishList"></div>			
	<div class="form-group" id="cityList"></div>	
	<div class="form-group" id="streetList"></div>		

	<div class="form-group">
		<label class="control-label col-sm-2" for="update"></label>        
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default" onClick="addOrUpdateAddress('` + parentform + `','` + addressId + `')" data-dismiss="modal">Update address</button>
		</div>
	</div>	
</form>
</div>`;

	getAllCounties(countyId);
	document.getElementById(htmlelement).innerHTML = content;
	fillStreet(countyId, parishId, cityId, street);
}


function getAllCounties(countyId) {
	$.getJSON("http://127.0.0.1:8080/PrivateLessons/rest/counties", function(counties){
		var selectorBody = document.getElementById("countySelect");
		var selectorContent = "<option></option>";
		var selected = "";
		for (var i = 0; i < counties.length; i++) {
			if (countyId == counties[i].id) {selected = "selected";}
			else (selected = "");
			
			selectorContent = selectorContent 
			+ "<option " + selected + " value=" 
			+ counties[i].id 
			+ ">"+counties[i].name 
			+ "</option>";
		}
		selectorBody.innerHTML = selectorContent;
	});
}

function fillStreet(countyId, parishId, cityId, street) {
	street = checkIfNullOrUndefined(street);
	
	document.getElementById("streetList").innerHTML  = `<label class="control-label col-sm-2" for= "streetSelect">Street, apt: </label>
														<div class="col-sm-3">
														<input class="form-control" type = "text" id = "streetSelect" value = "` + street + `">
														</div>`;  
	fillParishes(countyId, parishId);
	fillCities(countyId, cityId);
}

function fillParishes(countyId, parishId){
	var countyId = document.getElementById("countySelect").value;
	$.getJSON("http://127.0.0.1:8080/PrivateLessons/rest/parishes/" + countyId, function(parishes){
		var selectorContent = `<label class="control-label col-sm-2"  for = "Parish">Parish: </label>
								<div class="col-sm-3">
								<select class="form-control" id = "parishSelect">
								<option></option>`;
		var selected = "";
		for (var i = 0; i < parishes.length; i++) {
			if (parishId == parishes[i].id) {selected = "selected";}
			else (selected = "");
			selectorContent = selectorContent 
			+ "<option " + selected + " value=" 
			+ parishes[i].id 
			+ ">"+parishes[i].name 
			+ "</option>";
		}
		selectorContent = selectorContent + "</select></div>";
		document.getElementById("parishList").innerHTML = selectorContent;
		
	});
}


function fillCities(countyId, cityId){
	var countyId = document.getElementById("countySelect").value;
	$.getJSON("http://127.0.0.1:8080/PrivateLessons/rest/cities/" + countyId, function(cities){
		var selectorContent = `<label class="control-label col-sm-2"  for= "city">City: </label>
								<div class="col-sm-3">
								<select class="form-control" id = "citySelect">
								<option></option>`;
		var selected = "";
		for (var i = 0; i < cities.length; i++) {
			if (cityId == cities[i].id) {selected = "selected";}
			else (selected = "");
			selectorContent = selectorContent 
			+ "<option " + selected + " value=" 
			+ cities[i].id 
			+ ">"+cities[i].name 
			+ "</option>";
		}
		selectorContent = selectorContent + "</select> </div>";
		document.getElementById("cityList").innerHTML = selectorContent;
	});
	
}
